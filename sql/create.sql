SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;


CREATE TABLE availability (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX availability_exported_idx ON availability USING btree (exported) WHERE (exported = false);
CREATE INDEX availability_time_idx ON availability USING btree ("time");

CREATE TABLE delay (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX delay_exported_idx ON delay USING btree (exported) WHERE (exported = false);
CREATE INDEX delay_time_idx ON delay USING btree ("time");

CREATE TABLE gaps_count (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value integer NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX gaps_count_exported_idx ON gaps_count USING btree (exported) WHERE (exported = false);
CREATE INDEX gaps_count_time_idx ON gaps_count USING btree ("time");

CREATE TABLE gaps_interval (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX gaps_interval_exported_idx ON gaps_interval USING btree (exported) WHERE (exported = false);
CREATE INDEX gaps_interval_time_idx ON gaps_interval USING btree ("time");

CREATE TABLE gaps_length (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX gaps_length_exported_idx ON gaps_length USING btree (exported) WHERE (exported = false);
CREATE INDEX gaps_length_time_idx ON gaps_length USING btree ("time");

CREATE TABLE latency (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX latency_exported_idx ON latency USING btree (exported) WHERE (exported = false);
CREATE INDEX latency_time_idx ON latency USING btree ("time");

CREATE TABLE "offset" (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX offset_exported_idx ON "offset" USING btree (exported) WHERE (exported = false);
CREATE INDEX offset_time_idx ON "offset" USING btree ("time");

CREATE TABLE overlaps_count (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value integer NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX overlaps_count_exported_idx ON overlaps_count USING btree (exported) WHERE (exported = false);
CREATE INDEX overlaps_count_time_idx ON overlaps_count USING btree ("time");

CREATE TABLE overlaps_interval (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX overlaps_interval_exported_idx ON overlaps_interval USING btree (exported) WHERE (exported = false);
CREATE INDEX overlaps_interval_time_idx ON overlaps_interval USING btree ("time");

CREATE TABLE overlaps_length (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX overlaps_length_exported_idx ON overlaps_length USING btree (exported) WHERE (exported = false);
CREATE INDEX overlaps_length_time_idx ON overlaps_length USING btree ("time");

CREATE TABLE rms (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX rms_exported_idx ON rms USING btree (exported) WHERE (exported = false);
CREATE INDEX rms_time_idx ON rms USING btree ("time");

CREATE TABLE spikes_amplitude (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX spikes_amplitude_exported_idx ON spikes_amplitude USING btree (exported) WHERE (exported = false);
CREATE INDEX spikes_amplitude_time_idx ON spikes_amplitude USING btree ("time");

CREATE TABLE spikes_count (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value integer NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX spikes_count_exported_idx ON spikes_count USING btree (exported) WHERE (exported = false);
CREATE INDEX spikes_count_time_idx ON spikes_count USING btree ("time");

CREATE TABLE spikes_interval (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX spikes_interval_exported_idx ON spikes_interval USING btree (exported) WHERE (exported = false);
CREATE INDEX spikes_interval_time_idx ON spikes_interval USING btree ("time");

CREATE TABLE timing_quality (
    "time" timestamp with time zone NOT NULL,
    stream_id character varying(6)[] NOT NULL,
    value real NOT NULL,
    window_length real NOT NULL
);
CREATE INDEX timing_quality_exported_idx ON timing_quality USING btree (exported) WHERE (exported = false);
CREATE INDEX timing_quality_time_idx ON timing_quality USING btree ("time");
