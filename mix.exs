defmodule Tanuki.Mixfile do
  use Mix.Project

  def project do
    [app: :tanuki,
     version: "0.1.0",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    # Specify extra applications you'll use from Erlang/Elixir
    [extra_applications: [:httpoison, :logger],
     mod: {Tanuki.Application, []}]
  end

  defp deps do
    [
      {:ecto, ">=2.1.4"},
      {:httpoison, "~>0.12"},
      {:postgrex, ">=0.0.0"}
    ]
  end
end
