defmodule Seiscomp.WaveformQuality do
  use Ecto.Schema

  @primary_key {:_oid, :integer, []}

  schema "waveformquality" do
    field :m_waveformid_networkcode, :string
    field :m_waveformid_stationcode, :string
    field :m_waveformid_locationcode, :string
    field :m_waveformid_channelcode, :string
    field :m_windowlength, :float
    field :m_start, :utc_datetime
    field :m_end, :utc_datetime
    field :m_parameter, :string
    field :m_value, :float
  end
end
