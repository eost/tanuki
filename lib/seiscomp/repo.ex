defmodule Seiscomp.Repo do
  use Ecto.Repo, otp_app: :tanuki

  import Ecto.Query, only: [from: 2]

  def count do
    one from p in Seiscomp.WaveformQuality, select: [count(p._oid)]
  end

  def firsts(count, parameter \\ nil) do
    case parameter do
      nil -> all from p in Seiscomp.WaveformQuality, order_by: :m_start, limit: ^count
      _ -> all from p in Seiscomp.WaveformQuality, where: p.m_parameter == ^parameter, order_by: :m_start, limit: ^count
    end
  end
end
