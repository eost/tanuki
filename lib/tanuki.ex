defmodule Tanuki do
  require Logger

  defp middle_time(starttime, endtime) do
    [starttime, endtime]
    |> Enum.map(&DateTime.to_unix &1)
    |> Enum.sum
    |> div(2)
    |> DateTime.from_unix!
  end

  def convert(point) do
    data = %{
      time: middle_time(point.m_start, point.m_end),
      stream_id: [
        point.m_waveformid_networkcode,
        point.m_waveformid_stationcode,
        point.m_waveformid_locationcode,
        point.m_waveformid_channelcode
      ],
      value: point.m_value,
      window_length: point.m_windowlength
    }

    case point.m_parameter do
      "availability" -> struct(Metrics.Availability, data)
      "delay" -> struct(Metrics.Delay, data)
      "gaps count" -> struct(Metrics.GapsCount, %{data|:value => round(data.value)})
      "gaps interval" -> struct(Metrics.GapsInterval, data)
      "gaps length" -> struct(Metrics.GapsLength, data)
      "latency" -> struct(Metrics.Latency, data)
      "offset" -> struct(Metrics.Offset, data)
      "overlaps count" -> struct(Metrics.OverlapsCount, %{data|:value => round(data.value)})
      "overlaps interval" -> struct(Metrics.OverlapsInterval, data)
      "overlaps length" -> struct(Metrics.OverlapsLength, data)
      "rms" -> struct(Metrics.Rms, data)
      "spikes amplitude" -> struct(Metrics.SpikesAmplitude, data)
      "spikes count" -> struct(Metrics.SpikesCount, %{data|:value => round(data.value)})
      "spikes interval" -> struct(Metrics.SpikesInterval, data)
      "timing quality" -> struct(Metrics.TimingQuality, data)
    end
  end

  def metric_to_string(metric) do
    [
      metric.stream_id |> Enum.join("."),
      metric.__struct__,
      metric.value,
      "(" <> DateTime.to_iso8601(metric.time) <> ")"
    ] |> Enum.join(" ")
  end

  def migrate(point) do
    result = Seiscomp.Repo.transaction(fn ->
        case Seiscomp.Repo.delete(point) do
          {:ok, _} ->
            point
            |> convert
            |> Metrics.Repo.insert
          {:error, changeset} ->
            Seiscomp.Repo.rollback(changeset)
        end
      end)

    case result do
      {:ok, {:ok, metric}} -> Logger.info(metric_to_string(metric) <> " moved")
      {_, {_, metric}} -> Logger.error("Error while moving : " <> metric_to_string(metric))
    end
  end
end
