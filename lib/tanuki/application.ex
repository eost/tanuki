defmodule Tanuki.Application do
  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: true

    children =
      case Mix.env() do
        :prod -> [
            supervisor(Seiscomp.Repo, []),
            supervisor(Metrics.Repo, []),
            supervisor(Tanuki.Migrator, [])
          ]
        _ -> [
            supervisor(Seiscomp.Repo, []),
            supervisor(Metrics.Repo, []),
          ]
      end

    opts = [strategy: :one_for_one, name: Tanuki.Supervisor]

    Supervisor.start_link(children, opts)
  end
end
