defmodule Tanuki.Migrator do
  use GenServer

  def start_link do
    {:ok, _pid} = GenServer.start_link(__MODULE__, :ok)
  end

  defp schedule_work do
    Process.send_after(self(), :migrate, 1000)
  end

  def init(state) do
    schedule_work()
    {:ok, state}
  end

  def handle_info(:migrate, :ok) do
    Seiscomp.Repo.firsts(1000)
    |> Enum.map(&Tanuki.migrate(&1))

    schedule_work()

    {:noreply, :ok}
  end
end
