defmodule Metrics.Availability do
  use Ecto.Schema

  @primary_key false

  schema "availability" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.Delay do
  use Ecto.Schema

  @primary_key false

  schema "delay" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.GapsCount do
  use Ecto.Schema

  @primary_key false

  schema "gaps_count" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :integer
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.GapsInterval do
  use Ecto.Schema

  @primary_key false

  schema "gaps_interval" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.GapsLength do
  use Ecto.Schema

  @primary_key false

  schema "gaps_length" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.Latency do
  use Ecto.Schema

  @primary_key false

  schema "latency" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.Offset do
  use Ecto.Schema

  @primary_key false

  schema "offset" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.OverlapsCount do
  use Ecto.Schema

  @primary_key false

  schema "overlaps_count" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :integer
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.OverlapsInterval do
  use Ecto.Schema

  @primary_key false

  schema "overlaps_interval" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.OverlapsLength do
  use Ecto.Schema

  @primary_key false

  schema "overlaps_length" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.Rms do
  use Ecto.Schema

  @primary_key false

  schema "rms" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.SpikesAmplitude do
  use Ecto.Schema

  @primary_key false

  schema "spikes_amplitude" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.SpikesCount do
  use Ecto.Schema

  @primary_key false

  schema "spikes_count" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :integer
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.SpikesInterval do
  use Ecto.Schema

  @primary_key false

  schema "spikes_interval" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end

defmodule Metrics.TimingQuality do
  use Ecto.Schema

  @primary_key false

  schema "timing_quality" do
    field :time, :utc_datetime
    field :stream_id, {:array, :string}
    field :value, :float
    field :window_length, :float
    field :exported, :boolean
  end
end
