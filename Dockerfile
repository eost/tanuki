FROM alpine:3.6

COPY . /opt/tanuki

WORKDIR /opt/tanuki

RUN adduser -S -h /opt/tanuki tanuki \
 && chown -R tanuki /opt/tanuki \
 && apk --no-cache add elixir erlang-crypto erlang-tools erlang-syntax-tools erlang-runtime-tools erlang-xmerl

USER tanuki

ENV MIX_ENV=prod

RUN mix local.hex --force \
 && mix local.rebar --force \
 && mix deps.get \
 && mix deps.compile \
 && mix compile

CMD mix run --no-compile --no-halt
