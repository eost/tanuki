# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :logger, :console,
  format: "$time $metadata[$level] $levelpad$message\n"

config :tanuki, Seiscomp.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: System.get_env("TNK_SC3DB")

config :tanuki, Metrics.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: System.get_env("TNK_DB")

config :tanuki, ecto_repos: [Metrics.Repo, Seiscomp.Repo]

import_config "#{Mix.env}.exs"
