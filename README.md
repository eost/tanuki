# Tanuki

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `tanuki` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [{:tanuki, "~> 0.1.0"}]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/tanuki](https://hexdocs.pm/tanuki).


docker run -it --rm -v /home/fabien/renass-test:/var/lib/postgresql/data -p 5432:5432 postgres:9.6

docker run -it --rm -v /home/fabien/scquality-test:/var/lib/postgresql/data -p 5433:5432 postgres:9.6
